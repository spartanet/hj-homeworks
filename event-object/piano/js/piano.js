﻿function changeRegister(e) {
  if (!e.altKey && !e.shiftKey) {
    setRegister(middle);
  } else if (e.altKey) {
    setRegister(higher);
  } else if (e.shiftKey) {
    setRegister(lower);
  }
}

function playSound() {
  let player = this.querySelector('audio');
  player.play();
}

function setRegister(reg) {
  if (reg != currentRegister) {
    let buttons = document.getElementsByTagName('li');
    for (let i = 0; i < buttons.length; i++) {
      let btnPlayer = buttons[i].querySelector('audio');
      btnPlayer.src = 'sounds/' + registers[reg] + '/' + sounds[i] + '.mp3';
    }
    let ulTag = document.querySelector('ul');
    ulTag.classList.remove(registers[currentRegister]);
    ulTag.classList.add(registers[reg]);
    currentRegister = reg;
  }
}

const lower = 0;
const middle = 1;
const higher = 2;

let registers = ['lower', 'middle', 'higher'];
let currentRegister = null;
let sounds = ['first', 'second', 'third', 'fourth', 'fifth'];

let buttons = document.getElementsByTagName('li');
for (let btn of buttons) {
  btn.addEventListener('click', playSound);
}

document.addEventListener('keydown', changeRegister);
document.addEventListener('keyup', changeRegister);

setRegister(middle);
