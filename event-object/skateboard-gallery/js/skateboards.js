﻿function setSelected(e) {
  e.preventDefault();
  selectedPreview.classList.toggle('gallery-current');
  this.classList.toggle('gallery-current');
  selectedPreview = this;
  imageTag.src = this.href;
}

let navTag = document.getElementById('nav');
let selectedPreview = navTag.querySelector('a.gallery-current');
let imageTag = document.getElementById('view');

let previews = navTag.getElementsByTagName('a');
for (let p of previews) {
  p.addEventListener('click', setSelected);
}
