﻿const secretWordLength = 9;
const secretWordSpell = 'KeyY,KeyT,KeyN,KeyJ,KeyK,KeyJ,KeyU,KeyB,KeyZ';

let word = [];

document.addEventListener('keydown', function (e) {
  if (e.altKey && e.ctrlKey && e.code == 'KeyT') {
    document.querySelector('nav').classList.toggle('visible');
  }

  word.push(e.code);
  if (word.length > secretWordLength) {
      word.shift();
  }

  if (checkSecretWord()) {
    document.querySelector('div.secret').classList.add('visible');
  }
});

function checkSecretWord() {
  return secretWordSpell === word.join();
}
