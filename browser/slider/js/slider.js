﻿var images = ['airmax.png', 'airmax-jump.png', 'airmax-on-foot.png', 'airmax-playground.png', 'airmax-top-view.png'];
var current = 0;

function moveNextImage() {
    if (current == images.length) {
        current = 0;
    }

    var imgTag = document.getElementById('slider');

    imgTag.src = 'i/' + images[current++];
}

setInterval(moveNextImage, 5000);
