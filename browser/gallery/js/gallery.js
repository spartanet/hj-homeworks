﻿function setImage(n) {
    if (n < 0 && n >= images.length) {
        return;
    }

    var imgTag = document.getElementById('currentPhoto');

    imgTag.src = 'i/' + images[n];
}

function moveNext() {
    if (++current >= images.length) {
        current = 0;
    }

    setImage(current);
}

function movePrev() {
    if (--current < 0) {
        current = images.length-1;
    }

    setImage(current);
}

var images = ['breuer-building.jpg', 'guggenheim-museum.jpg', 'headquarters.jpg', 'IAC.jpg', 'new-museum.jpg'];
var current = 0;

window.prevPhoto.onclick = movePrev;
window.nextPhoto.onclick = moveNext;

setImage(current);
