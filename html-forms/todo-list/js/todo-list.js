﻿function getCheckedCount() {
  return document.querySelectorAll('input[type=checkbox]:checked').length;
}

function updateTaskProgress() {
  var checkedCount = getCheckedCount();
  taskProgressBox.value = `${checkedCount} из ${allCheckboxesCount}`;

  if (allCheckboxesCount == checkedCount) {
    container.classList.add('complete');
  } else {
    container.classList.remove('complete');
  }
}

function handleCheck(e) {
  if (e.target.tagName == 'INPUT' && e.target.type == 'checkbox') {
    updateTaskProgress();
  }
}

var allCheckboxesCount = document.querySelectorAll('input[type=checkbox]').length;
var taskProgressBox = document.querySelector('output');
var container = document.querySelector('.list-block');
container.addEventListener('click', handleCheck);

updateTaskProgress();
