﻿function validatePostcode(e) {
  if (e instanceof KeyboardEvent && e.key.length == 1 && !/\d/.test(e.key)) {
    e.preventDefault();
  }
}

function checkForm(e) {
  if (e.target.tagName == 'INPUT' || e.target.tagName == 'TEXTAREA') {
    var output = document.querySelector('#' + e.target.name);
    if (output != null) {
      output.value = e.target.value;
    }
  }

  var isFormFilledIn = true;
  for (const input of allFields) {
    isFormFilledIn &= input.value.length > 0;
  }

  submitBtn.disabled = !isFormFilledIn;
}

function toggleView(e) {
  e.preventDefault();
  form.classList.toggle('hidden');
  messageView.classList.toggle('hidden');
}

var form = document.querySelector('.contentform');
var messageView = document.querySelector('#output');
var allFields = form.querySelectorAll('input, textarea');
var submitBtn = form.querySelector('button[type=submit]');
var editBtn = messageView.querySelector('button');
var postcodeInput = form.querySelector('input[name=zip]');

form.addEventListener('input', checkForm);
postcodeInput.addEventListener('keydown', validatePostcode);
submitBtn.addEventListener('click', toggleView);
editBtn.addEventListener('click', toggleView);
