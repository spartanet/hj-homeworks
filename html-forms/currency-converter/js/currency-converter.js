﻿function initCurrencyLists() {
  if (xhr.status !== 200) {
    console.log(`Ответ ${xhr.status}: ${xhr.statusText}`);
    return;
  }

  var currencyList = JSON.parse(xhr.responseText);
  for (const item of currencyList) {
    var optionTag = document.createElement('option');
    optionTag.textContent = item.code;
    optionTag.value = item.value;
    fromSelect.appendChild(optionTag);
    toSelect.appendChild(optionTag.cloneNode(true));
  }

  updateOutput();

  form.classList.remove('hidden');
}

function changeLoadStatus() {
  preloader.classList.toggle('hidden');
}

function updateOutput() {
  var result = parseFloat(input.value);
  result = isNaN(result) ? 0 : fromSelect.value / toSelect.value * result;
  output.value = result.toFixed(2);
}

var preloader = document.querySelector('#loader');
var form = document.querySelector('#content');
var fromSelect = document.querySelector('#from');
var toSelect = document.querySelector('#to');
var output = document.querySelector('#result');
var input = document.querySelector('#source');
input.addEventListener('input', updateOutput);
fromSelect.addEventListener('change', updateOutput);
toSelect.addEventListener('change', updateOutput);

var xhr = new XMLHttpRequest();
xhr.open('GET', 'https://neto-api.herokuapp.com/currency', true);
xhr.addEventListener('loadstart', changeLoadStatus);
xhr.addEventListener('loadend', changeLoadStatus);
xhr.addEventListener('load', initCurrencyLists);
xhr.send();
