﻿var songs = ['LA Chill Tour.mp3', 'LA Fusion Jam.mp3', 'This is it band.mp3'];
var i = 0;

var buttonsTag = document.querySelector('.buttons');
var player = document.querySelector('.mediaplayer');
var audio = player.getElementsByTagName('audio')[0];
audio.addEventListener('ended', function () {
  toggleAnimation(false);
})
var titleTag = player.querySelector('.title');
var isPlayed = false;

var playBtn = buttonsTag.querySelector('.playstate');
playBtn.addEventListener('click', playBtnClickHandler);

var stopBtn = buttonsTag.querySelector('.stop');
stopBtn.addEventListener('click', stop);

var skipNextBtn = buttonsTag.querySelector('.next');
skipNextBtn.addEventListener('click', skipNext);

var skipPrevBtn = buttonsTag.querySelector('.back');
skipPrevBtn.addEventListener('click', skipPrevious);

setSongPath(i);

function playBtnClickHandler() {
  isPlayed = !isPlayed;

  if (isPlayed) {
    play();
  } else {
    pause();
  }
}

function play() {
  toggleAnimation(true);
  audio.play();
}

function pause() {
  toggleAnimation(false);
  audio.pause();
}

function stop() {
  pause();
  audio.currentTime = 0;
}

function skipNext() {
  if (++i >= songs.length) {
    i = 0;
  }

  setSongPath(i);
  play();
}

function skipPrevious() {
  if (--i < 0) {
    i = songs.length-1;
  }

  setSongPath(i);
  play();
}

function setSongPath(index) {
  if (index < 0 || index >= songs.length)
    return;

  var filename = songs[index];
  var fnParts = filename.split('.');

  titleTag.title = fnParts[0];

  audio.src = 'mp3/' + filename;
}

function toggleAnimation(set) {
  isPlayed = set;

  if (set) {
    player.classList.add('play');
  } else {
    player.classList.remove('play');
  }
}
