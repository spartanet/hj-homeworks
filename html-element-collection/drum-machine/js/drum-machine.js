﻿var buttons = document.getElementsByClassName('drum-kit__drum');
for (var btn of buttons) {
  btn.addEventListener('click', function () {
    var audio = this.getElementsByTagName('audio')[0];
    audio.play();
  });
}
