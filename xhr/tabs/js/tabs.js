﻿function showTab(tabObj) {
  currentTab.classList.toggle('active');
  tabObj.classList.toggle('active');

  content.innerHTML = '';
  preloader.classList.toggle('hidden');
  currentTab = tabObj;

  let xhr = new XMLHttpRequest();
  xhr.addEventListener('load', function () {
    if (this.status == 200) {
      preloader.classList.toggle('hidden');
      content.innerHTML = this.responseText;
    }
  });
  xhr.open('GET', tabObj.href, true);
  xhr.send();
}

function tabClickHandler(e) {
  e.preventDefault();
  showTab(this);
}

var currentTab = document.querySelector('nav > a.active');
var preloader = document.querySelector('#preloader');
var content = document.querySelector('#content');

for (const tab of document.querySelectorAll('nav > a')) {
  tab.addEventListener('click', tabClickHandler);
}

showTab(currentTab);
