﻿function generateCatalog () {
  if (this.status != 200) {
    return;
  }

  let ulTag = document.querySelector('#content');
  let books = JSON.parse(this.responseText);
  ulTag.innerHTML = Array.from(books).reduce(function (prev, book) {
    return prev += `<li data-title="${book.title}" data-author="${book.author.name}" data-info="${book.info}" data-price="${book.price}"><img src="${book.cover.small}" ></li>`;
  }, '');
}
xhr = new XMLHttpRequest();
xhr.addEventListener('load', generateCatalog);
xhr.open('GET', 'https://netology-fbb-store-api.herokuapp.com/book/', true);
xhr.send();
