const request = new XMLHttpRequest();
request.open('GET', 'https://netology-fbb-store-api.herokuapp.com/weather', true);
request.addEventListener('load', loadDataIntoWidget);
request.send();

function loadDataIntoWidget() {
  if (this.status === 200) {
    const response = JSON.parse(this.responseText);
    setData(response);
  }
}
