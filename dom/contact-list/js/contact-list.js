﻿var contacts = Array.from(JSON.parse(loadContacts()));
var listTag = document.querySelector('.contacts-list');
listTag.innerHTML = contacts.reduce(function(prev, contact) {
  return prev += `<li data-email="${contact.email}" data-phone="${contact.phone}"><strong>${contact.name}</strong></li>`
}, '');
