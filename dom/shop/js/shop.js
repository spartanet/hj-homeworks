﻿let goodsCountTag = document.querySelector('#cart-count');
let goodsSumTag = document.querySelector('#cart-total-price');
goodsSumTag.dataset.totalSum = goodsSumTag.innerHTML;

function addToCart() {
  goodsCountTag.innerHTML++;
  goodsSumTag.dataset.totalSum = +goodsSumTag.dataset.totalSum + +this.dataset.price;
  goodsSumTag.innerHTML = getPriceFormatted(goodsSumTag.dataset.totalSum);
}

let buttons = document.querySelectorAll('.add');
for (let btn of buttons) {
  btn.addEventListener('click', addToCart);
}
